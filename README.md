# Treeslator

TreeSlator is a simple English-Spanish dictionary program implemented in Java.

## Dependencies

### Java
#### Debian
    
```bash
    sudo apt install default-jdk
    sudo apt install default-jre
```
#### Arch Linux
    
```bash
    sudo pacman -S jdk-openjdk  
    sudo pacman -S jre-openjdk
```
#### Fedora
```bash
    sudo dnf install java-11-openjdk-devel
    sudo dnf install java-11-openjdk
```
#### Gentoo
```bash
    sudo emerge --ask virtual/jdk
    sudo emerge --ask virtual/jre
```

## Installation

To install TreeSlator, follow these steps:
- Clone this repository
    ```bash
    git clone https://codeberg.org/juampam/treelastor.git
    ```
- Run the installation script:
    ```bash
    cd treeslator
    chmod +x install.sh
    ./install.sh
    ```

## Usage

Once installed, you can use TreeSlator to translate English words or sentences to Spanish. Here's how it works:

1. Open a terminal.
2. Type the following command to execute the program:
    ```bash
    treeslator <file>
    ```
- Create a file
    ```bash
    printf "%s\n" "The house is big and beautiful" \
                "The dog is barking loudly" \
                "I have a lot of homework to do" \
                "The woman is walking in the town" \
                "Yes I will go to the party." > translateme.txt
    ```
- now run
    ```bash
    treeslator translateme.txt
    ```

## Features

- English-Spanish dictionary lookup.
- Translation of English text to Spanish.
- Simple command-line interface.

## Overview
### `TreeNode` Class:
- Represents a node in the binary search tree (BST).
- Attributes:
  - `key`: Key value of the node (e.g., English word).
  - `value`: Value associated with the key (e.g., Spanish translation).
  - `left`: Points to the left child node.
  - `right`: Points to the right child node.
- Constructor initializes key, value, left, and right attributes.

### `BinarySearchTree` Class:
- Represents the binary search tree data structure.
- Contains reference to the root node of the tree.
- Constructor initializes root to `null`.
- Methods:
  - `insert(String key, String value)`: Inserts a new node into the binary search tree.
  - `insertRecursive(TreeNode root, String key, String value)`: Recursive method to insert a node.
  - `inorderTraversal(TreeNode node)`: Performs in-order traversal of the tree.
  - `search(String key)`: Searches for a node with the given key.
  - `searchRecursive(TreeNode root, String key)`: Recursive method to search for a node.

### `Dictionary` Class:
- Contains `main` method and utility methods to build the dictionary and translate text.
- `DICTIONARY_FILE_PATH`: File path of the dictionary.
- `buildDictionary()`: Reads dictionary file and constructs binary search tree.
- `translateText(BinarySearchTree dictionary, String[] lines)`: Translates text using binary search tree dictionary.
- `main(String[] args)`: Entry point of the program. Reads text file, translates text, and prints translated text.

## Unistall
To remove executable scripts from the system run:

```bash
sudo rm /usr/local/share/translator
sudo rm -rf /usr/local/share/translator
```
