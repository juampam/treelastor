#!/bin/bash

INSTALL_DIR="/usr/local/bin"
DICTIONARY_DIR="/usr/local/share/treeslator"
sudo mkdir $DICTIONARY_DIR
sudo sed -i "s|DICTIONARY_FILE_PATH = .*|DICTIONARY_FILE_PATH = \"$DICTIONARY_DIR/dictionary\";|" src/Dictionary.java

javac src/*.java

echo "Main-Class: Dictionary" > src/manifest.txt

cd src || exit
jar cvfm treeslator.jar manifest.txt *.class

sudo mv treeslator.jar "$DICTIONARY_DIR"

echo "#!/bin/bash" > "treeslator"
echo "java -jar $DICTIONARY_DIR/treeslator.jar \"\$@\"" >> "treeslator"
chmod +x treeslator 
sudo mv treeslator $INSTALL_DIR


echo "Done"
echo "Usage: treeslator <file_path>"

