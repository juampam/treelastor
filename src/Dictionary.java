import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

class TreeNode {
    String key;
    String value;
    TreeNode left;
    TreeNode right;

    public TreeNode(String key, String value) {
        this.key = key;
        this.value = value;
        left = null;
        right = null;
    }
}

class BinarySearchTree {
    TreeNode root;

    public BinarySearchTree() {
        root = null;
    }

    void insert(String key, String value) {
        root = insertRecursive(root, key, value);
    }

    TreeNode insertRecursive(TreeNode root, String key, String value) {
        if (root == null) {
            return new TreeNode(key, value);
        }

        if (key.compareToIgnoreCase(root.key) < 0) {
            root.left = insertRecursive(root.left, key, value);
        } else if (key.compareToIgnoreCase(root.key) > 0) {
            root.right = insertRecursive(root.right, key, value);
        } else {
            root.value = value;
        }

        return root;
    }

    void inorderTraversal(TreeNode node) {
        if (node != null) {
            inorderTraversal(node.left);
            System.out.print("(" + node.key + ", " + node.value + ") ");
            inorderTraversal(node.right);
        }
    }

    String search(String key) {
        return searchRecursive(root, key);
    }

    String searchRecursive(TreeNode root, String key) {
        if (root == null || root.key.equalsIgnoreCase(key)) {
            return (root != null) ? root.value : null;
        }

        if (key.compareToIgnoreCase(root.key) < 0) {
            return searchRecursive(root.left, key);
        } else {
            return searchRecursive(root.right, key);
        }
    }
}

public class Dictionary {
    static final String DICTIONARY_FILE_PATH = "/usr/local/share/treeslator/dictionary";

    public static BinarySearchTree buildDictionary() throws IOException {
        BinarySearchTree dictionary = new BinarySearchTree();

        try (BufferedReader br = new BufferedReader(new FileReader(DICTIONARY_FILE_PATH))) {
            String line;
            while ((line = br.readLine()) != null) {
                line = line.trim();
                String[] parts = line.substring(1, line.length() - 1).split(",");
                String key = parts[0].trim().toLowerCase();
                String value = parts[1].trim().toLowerCase();
                dictionary.insert(key, value);
            }
        }

        return dictionary;
    }

    public static String[] translateText(BinarySearchTree dictionary, String[] lines) {
        String[] translatedLines = new String[lines.length];

        for (int i = 0; i < lines.length; i++) {
            if (lines[i] != null) {
                String[] words = lines[i].split(" ");
                StringBuilder translatedLine = new StringBuilder();

                for (String word : words) {
                    String translation = dictionary.search(word.toLowerCase());
                    if (translation != null) {
                        translatedLine.append(translation).append(" ");
                    } else {
                        translatedLine.append("*").append(word).append("* ");
                    }
                }

                translatedLines[i] = translatedLine.toString().trim();
            }
        }

        return translatedLines;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage: java Dictionary <text_file_path>");
            return;
        }

        String textFilePath = args[0];

        try {
            BinarySearchTree dictionary = buildDictionary();

            BufferedReader br = new BufferedReader(new FileReader(textFilePath));
            String line;
            String[] lines = new String[100]; // Adjust size as needed
            int lineCount = 0;
            while ((line = br.readLine()) != null) {
                lines[lineCount++] = line;
            }
            br.close();

            String[] translatedLines = translateText(dictionary, lines);
            for (String translatedLine : translatedLines) {
                if (translatedLine != null) {
                    System.out.println(translatedLine);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

